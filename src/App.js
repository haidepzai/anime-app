import React from "react";
import { AnimeProvider } from "./context/AnimeContext";
import AnimeList from "./components/AnimeList";
import {
  CssBaseline,
  AppBar,
  Toolbar,
  Typography,
  Container,
} from "@mui/material";

function App() {
  return (
    <AnimeProvider>
      <CssBaseline />
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6">Anime Database</Typography>
        </Toolbar>
      </AppBar>
      <Container style={{ marginTop: "20px" }}>
        <AnimeList />
      </Container>
    </AnimeProvider>
  );
}

export default App;
