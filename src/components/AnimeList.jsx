import React, { useContext } from "react";
import { AnimeContext } from "../context/AnimeContext";
import { Grid, CircularProgress, Container, Typography } from "@mui/material";
import AnimeItem from "./AnimeItem";
import useInfiniteScroll from "../hooks/useInfiniteScroll";

const AnimeList = () => {
  const { animeList, loading, loadMore } = useContext(AnimeContext);
  useInfiniteScroll(loadMore);

  if (!loading && animeList.length === 0) {
    return (
      <Container
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "80vh",
        }}
      >
        <Typography variant="h6">
          No anime found. Please try again later.
        </Typography>
      </Container>
    );
  }

  return (
    <Container>
      <Grid container spacing={3}>
        {animeList.map((anime) => (
          <Grid item xs={12} sm={6} md={4} lg={3} key={anime.mal_id}>
            <AnimeItem anime={anime} />
          </Grid>
        ))}
      </Grid>
      {loading && (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            marginTop: "20px",
          }}
        >
          <CircularProgress />
        </div>
      )}
    </Container>
  );
};

export default AnimeList;
