import React, { useState } from "react";
import {
  Card,
  CardMedia,
  CardContent,
  Typography,
  Button,
  CardActions,
} from "@mui/material";

const AnimeItem = ({ anime }) => {
  const [showMore, setShowMore] = useState(false);
  const toggleShowMore = () => setShowMore(!showMore);

  const getShortSynopsis = (synopsis) => {
    return synopsis.length > 100
      ? synopsis.substring(0, 100) + "..."
      : synopsis;
  };

  return (
    <Card>
      <CardMedia
        component="img"
        alt={anime.title}
        height="300"
        image={anime.images.jpg.image_url}
      />
      <CardContent>
        <Typography variant="h6">{anime.title}</Typography>
        <Typography variant="body2" color="textSecondary">
          {showMore ? anime.synopsis : getShortSynopsis(anime.synopsis)}
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small" color="primary" onClick={toggleShowMore}>
          {showMore ? "Show Less" : "Show More"}
        </Button>
      </CardActions>
    </Card>
  );
};

export default AnimeItem;
