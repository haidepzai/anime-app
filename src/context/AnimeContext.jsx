import React, { createContext, useState, useEffect } from 'react';
import axios from 'axios';

export const AnimeContext = createContext();

export const AnimeProvider = ({ children }) => {
  const [animeList, setAnimeList] = useState([]);
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(false);

  const fetchAnime = async (page) => {
    setLoading(true);
    try {
      const response = await axios.get(`https://api.jikan.moe/v4/anime?page=${page}`);
      setAnimeList((prev) => [...prev, ...response.data.data]);
      setLoading(false);
    } catch (error) {
      console.error('Error fetching data: ', error);
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchAnime(page);
  }, [page]);

  const loadMore = () => {
    setPage((prev) => prev + 1);
  };

  return (
    <AnimeContext.Provider value={{ animeList, loading, loadMore }}>
      {children}
    </AnimeContext.Provider>
  );
};
